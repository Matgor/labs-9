/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author st
 */
public class Term implements pk.labs.Lab9.beans.Term, Serializable{

    public Term() {
    }
    private Date begin;
    private int duration;
    private Date end;

    
    
    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public Date getBegin() {
       return begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public void setDuration(int duration) {
        if(duration>0)
            this.duration = duration;
    }

    @Override
    public Date getEnd() {
        Date end = new Date(begin.getTime() + duration*60*1000);
        return end;
    }
}
