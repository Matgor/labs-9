/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import pk.labs.Lab9.beans.ConsultationList;
import java.io.*;
/**
 *
 * @author st
 */
public class ConsultationListFactory implements pk.labs.Lab9.beans.ConsultationListFactory, Serializable{
    
    private pk.labs.Lab9.beans.impl.ConsultationList consultationList;

    /**
     * Get the value of consultationList
     *
     * @return the value of consultationList
     */
    public pk.labs.Lab9.beans.impl.ConsultationList getConsultationList() {
        return consultationList;
    }

    /**
     * Set the value of consultationList
     *
     * @param consultationList new value of consultationList
     */
    public void setConsultationList(pk.labs.Lab9.beans.impl.ConsultationList consultationList) {
        this.consultationList = consultationList;
    }

    @Override
    public ConsultationList create() {
        return consultationList;
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        return create();
        //if (deserialize = false)
       //     return consultationList;
       // else
        //    return null;
    }

    @Override
    public void save(ConsultationList consultationList) {
          try {
              try (ObjectOutputStream out = new ObjectOutputStream(
                   new FileOutputStream("test.ser")
                   )) {
                  out.writeObject(consultationList);
              }
            } catch(IOException exc) {
            System.exit(1);
            }  
    }
    
}
