/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.beans.*;
import pk.labs.Lab9.beans.Consultation;

/**
 *
 * @author st
 */
public class ConsultationList implements pk.labs.Lab9.beans.ConsultationList, Serializable{

    public ConsultationList() {
        this.pcs = new PropertyChangeSupport(this);
        this.consultation = new Consultation[100];
    }
    private Consultation[] consultation;
    private PropertyChangeSupport pcs;
    
    
    @Override
    public int getSize() {
        return consultation.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return consultation;
    }

    @Override
    public Consultation getConsultation(int index) {
        return consultation[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        for (int i = 0; i < this.getSize(); i++) {
            if (consultation.getBeginDate().before(this.consultation[i].getEndDate())|| consultation.getEndDate().after(this.consultation[i].getBeginDate())) {
                throw new PropertyVetoException("Konflikt z innymi konsultacjami", new PropertyChangeEvent(pcs, null, i, i));
            }
                
            
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }
    
    
}
