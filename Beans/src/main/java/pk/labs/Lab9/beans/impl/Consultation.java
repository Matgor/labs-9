/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author st
 */
public class Consultation implements pk.labs.Lab9.beans.Consultation, Serializable{
    private String student;
    private Term term;
    private VetoableChangeSupport vcs = new VetoableChangeSupport(this);;
    
    
    
    @Override
    public String getStudent() {
        return student;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException{
        vcs.fireVetoableChange("term", this.term, term);
        this.term = term;      
    }
    
     public void addVetoableChangeListener(VetoableChangeListener listener) {  
        vcs.addVetoableChangeListener(listener);  
    }  
    public void removeVetoableChangeListener(VetoableChangeListener listener) {  
        vcs.removeVetoableChangeListener(listener);  
    }  
    
    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
